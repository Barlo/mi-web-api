﻿using AutoMapper;

using Mi.Infrastructure.WebApi.Context;
using Mi.Infrastructure.WebApi.Model;

namespace Mi.Infrastructure.WebApi.Controllers {
    public class RecipeStepController : Controller<RecipeStep, RecipeStep> {
        public RecipeStepController(MiDbContext miDbContext, IMapper mapper) : 
            base(miDbContext, miDbContext.RecipeSteps, mapper) { }
    }
}
