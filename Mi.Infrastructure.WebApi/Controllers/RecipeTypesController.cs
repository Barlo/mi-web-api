﻿using AutoMapper;

using Mi.Infrastructure.WebApi.Context;
using Mi.Infrastructure.WebApi.Model;

using Microsoft.AspNetCore.Mvc;

namespace Mi.Infrastructure.WebApi.Controllers {
    [Route("api/[controller]")]
    [ApiController]
    public class RecipeTypesController : Controller<RecipeType, RecipeType> {
        public RecipeTypesController(MiDbContext miDbContext, IMapper mapper) : 
            base(miDbContext, miDbContext.RecipeTypes, mapper) { }
    }
}
