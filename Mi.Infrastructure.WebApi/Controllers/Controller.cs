﻿
using AutoMapper;

using Mi.Infrastructure.WebApi.Context;
using Mi.Infrastructure.WebApi.Model.DatabaseModel;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace Mi.Infrastructure.WebApi.Controllers {
    [Route("api/[controller]")]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ApiController]
    public class Controller<T, TNew> : ControllerBase
        where T : Entity
        where TNew : class {
        private readonly MiDbContext context;
        private readonly DbSet<T> dbSet;
        private readonly IMapper mapper;

        public Controller(MiDbContext context,
            DbSet<T> dbSet, IMapper mapper) {
            this.context = context;
            this.dbSet = dbSet;
            this.mapper = mapper;
        }

        [HttpGet()]
        public async Task<IEnumerable<T>> Get() => await this.dbSet.ToListAsync();

        [HttpGet("{id}")]
        public async Task<T> Get(int id) => await this.dbSet.FirstAsync(x => x.Id == id);

        [HttpPut("{id}")]
        public async Task Put(int id, [FromBody] TNew entity) {
            var updatedEntity = this.mapper.Map<T>(entity);

            updatedEntity.Id = id;

            _ = this.dbSet.Update(updatedEntity);

            _ = await this.context.SaveChangesAsync();

        }

        [HttpPost]
        public async Task Post(TNew entity) {
            _ = this.dbSet.Add(this.mapper.Map<T>(entity));

            _ = await this.context.SaveChangesAsync();
        }

        [HttpDelete("{id}")]
        public async Task Delete(int id) {
            var deleteEntity = await this.dbSet.FirstAsync(x => x.Id == id);

            _ = this.dbSet.Remove(deleteEntity);

            _ = await this.context.SaveChangesAsync();

        }
    }
}
