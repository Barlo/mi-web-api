﻿using AutoMapper;

using Mi.Infrastructure.WebApi.Context;
using Mi.Infrastructure.WebApi.Model.DatabaseModel;

using Microsoft.AspNetCore.Mvc;

namespace Mi.Infrastructure.WebApi.Controllers {
    [Route("api/[controller]")]
    [ApiController]
    public class ToDoController : Controller<ToDo, ToDo> {
        public ToDoController(MiDbContext miDbContext, IMapper mapper) : 
            base(miDbContext, miDbContext.ToDo, mapper) { }
    }
}
