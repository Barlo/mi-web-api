﻿// <auto-generated />
using System;
using Mi.Infrastructure.WebApi.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Mi.Infrastructure.WebApi.Migrations
{
    [DbContext(typeof(MiDbContext))]
    [Migration("20201129220948_recipesAndAquaMeasurements")]
    partial class recipesAndAquaMeasurements
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .UseIdentityColumns()
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.0");

            modelBuilder.Entity("Mi.Infrastructure.WebApi.Model.Aquarium", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("Size")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.ToTable("Aquariums");
                });

            modelBuilder.Entity("Mi.Infrastructure.WebApi.Model.AquariumMeasurement", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<int?>("AquariumId")
                        .HasColumnType("int");

                    b.Property<double>("Chsk")
                        .HasColumnType("float");

                    b.Property<double>("Co2")
                        .HasColumnType("float");

                    b.Property<DateTime>("Date")
                        .HasColumnType("datetime2");

                    b.Property<double>("Gh")
                        .HasColumnType("float");

                    b.Property<double>("Kh")
                        .HasColumnType("float");

                    b.Property<double>("Nh3")
                        .HasColumnType("float");

                    b.Property<double>("No3")
                        .HasColumnType("float");

                    b.Property<double>("Ph")
                        .HasColumnType("float");

                    b.Property<double>("Po4")
                        .HasColumnType("float");

                    b.HasKey("Id");

                    b.HasIndex("AquariumId");

                    b.ToTable("AquariumsMeasurements");
                });

            modelBuilder.Entity("Mi.Infrastructure.WebApi.Model.Recipe", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<string>("Author")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int?>("RecipeTypeId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("RecipeTypeId");

                    b.ToTable("Recipes");
                });

            modelBuilder.Entity("Mi.Infrastructure.WebApi.Model.RecipeStep", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<string>("Description")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int?>("RecipeId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("RecipeId");

                    b.ToTable("RecipeSteps");
                });

            modelBuilder.Entity("Mi.Infrastructure.WebApi.Model.RecipeType", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<string>("Type")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("RecipeTypes");
                });

            modelBuilder.Entity("Mi.Infrastructure.WebApi.Model.AquariumMeasurement", b =>
                {
                    b.HasOne("Mi.Infrastructure.WebApi.Model.Aquarium", "Aquarium")
                        .WithMany("AquariumMeasurements")
                        .HasForeignKey("AquariumId");

                    b.Navigation("Aquarium");
                });

            modelBuilder.Entity("Mi.Infrastructure.WebApi.Model.Recipe", b =>
                {
                    b.HasOne("Mi.Infrastructure.WebApi.Model.RecipeType", "RecipeType")
                        .WithMany("Recipes")
                        .HasForeignKey("RecipeTypeId");

                    b.Navigation("RecipeType");
                });

            modelBuilder.Entity("Mi.Infrastructure.WebApi.Model.RecipeStep", b =>
                {
                    b.HasOne("Mi.Infrastructure.WebApi.Model.Recipe", "Recipe")
                        .WithMany("RecipeSteps")
                        .HasForeignKey("RecipeId");

                    b.Navigation("Recipe");
                });

            modelBuilder.Entity("Mi.Infrastructure.WebApi.Model.Aquarium", b =>
                {
                    b.Navigation("AquariumMeasurements");
                });

            modelBuilder.Entity("Mi.Infrastructure.WebApi.Model.Recipe", b =>
                {
                    b.Navigation("RecipeSteps");
                });

            modelBuilder.Entity("Mi.Infrastructure.WebApi.Model.RecipeType", b =>
                {
                    b.Navigation("Recipes");
                });
#pragma warning restore 612, 618
        }
    }
}
