﻿using AutoMapper;

using Mi.Infrastructure.WebApi.Model.Profiles;

using Microsoft.Extensions.DependencyInjection;

namespace Mi.Infrastructure.WebApi.Middleware {
    public static class MapperMiddleware {
        public static IServiceCollection AddMapper(this IServiceCollection serviceCollection) {
            var mapperConfig = new MapperConfiguration(mapperConfig => {
                mapperConfig.AddProfile(new MappingProfile());
            });

            IMapper mapper = mapperConfig.CreateMapper();

            serviceCollection.AddSingleton(mapper);

            return serviceCollection;
        }
    }
}
