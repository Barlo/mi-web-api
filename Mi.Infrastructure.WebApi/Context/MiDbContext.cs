﻿using Mi.Infrastructure.WebApi.Model;
using Mi.Infrastructure.WebApi.Model.DatabaseModel;

using Microsoft.EntityFrameworkCore;

namespace Mi.Infrastructure.WebApi.Context {
    public class MiDbContext : DbContext {
        public DbSet<Aquarium> Aquariums { get; set; }
        public DbSet<AquariumMeasurement> AquariumsMeasurements { get; set; }
        public DbSet<Device> Devices { get; set; }
        public DbSet<Recipe> Recipes { get; set; }
        public DbSet<RecipeStep> RecipeSteps { get; set; }
        public DbSet<RecipeType> RecipeTypes { get; set; }
        public DbSet<ToDo> ToDo { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) {
            optionsBuilder.UseSqlServer("Server=tcp:midatabaseserver.database.windows.net,1433;Initial Catalog=mi_db;Persist Security Info=False;User ID=barlo;Password=dawid12345!;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
        }
    }
}
