﻿using System;
using System.Collections.Generic;

namespace Mi.Infrastructure.WebApi.Model.DatabaseModel {
    public class CalendarDay : Entity{
        public DateTime Date { get; set; }
        public IList<DayEvent> DayEvents { get; set; } = new List<DayEvent>();
    }
}
