﻿using Mi.Infrastructure.WebApi.Model.DatabaseModel;

using System;

namespace Mi.Infrastructure.WebApi.Model {
    public class AquariumMeasurement : Entity{
        public DateTime Date { get; set; }
        public virtual Aquarium Aquarium { get; set; }
        public double Ph { get; set; }
        public double Gh { get; set; }
        public double Kh{ get; set; }
        public double Co2 { get; set; }
        public double Nh3 { get; set; }
        public double No3 { get; set; }
        public double Po4 { get; set; }
        public double Chsk { get; set; }
    }
}
