﻿using Mi.Infrastructure.WebApi.Model.DatabaseModel;

namespace Mi.Infrastructure.WebApi.Model {
    public class RecipeStep : Entity{
        public string Description { get; set; }
        public virtual Recipe Recipe { get; set; }
    }
}
