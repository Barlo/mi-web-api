﻿namespace Mi.Infrastructure.WebApi.Model.DatabaseModel {
    public class ConfigurationElement : Entity{
        public string Name { get; set; }
        public virtual Configuration Configuration { get; set; }
        public string Value { get; set; }
        public string Type { get; set; }
    }
}
