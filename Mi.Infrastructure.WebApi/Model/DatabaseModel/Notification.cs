﻿using System;

namespace Mi.Infrastructure.WebApi.Model.DatabaseModel {
    public class Notification : Entity{
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime? Date { get; set; }
    }
}
