﻿namespace Mi.Infrastructure.WebApi.Model.Extensions {
    public static class AquariumExtensions {
        public static void Deconstruct(this Aquarium aquarium, out int id, out string name, out int size) {
            id = aquarium.Id;
            name = aquarium.Name;
            size = aquarium.Size;
        }
    }
}
