﻿using System;

namespace Mi.Infrastructure.WebApi.Model.DatabaseModel {
    public class DayEvent : Entity{
        public string Name { get; set; }
        public virtual CalendarDay CalendarDay { get; set; }
        public bool EntireDay { get; set; }
        public DateTime? TimeFrom { get; set; }
        public DateTime? TimeTo { get; set; }
    }
}