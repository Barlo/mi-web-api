﻿using Mi.Infrastructure.WebApi.Model.DatabaseModel;

using System.Collections.Generic;

namespace Mi.Infrastructure.WebApi.Model {
    public class RecipeType : Entity{
        public string Type { get; set; }
        public virtual IList<Recipe> Recipes { get; set; } = new List<Recipe>();
    }
}
