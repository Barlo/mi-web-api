﻿using System.Collections.Generic;

namespace Mi.Infrastructure.WebApi.Model.DatabaseModel {
    public class Configuration : Entity{
        public string Name { get; set; }
        public IList<ConfigurationElement> ConfigurationElements { get; set; } = new List<ConfigurationElement>();
    }
}
