﻿using System;

namespace Mi.Infrastructure.WebApi.Model.DatabaseModel {
    public class ToDo : Entity{
        public string Title { get; set; }
        public string Descrition { get; set; }
        public string Author { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
