﻿using Mi.Infrastructure.WebApi.Model.DatabaseModel;

using System.Collections.Generic;

namespace Mi.Infrastructure.WebApi.Model {
    public class Recipe : Entity{
        public string Name { get; set; }
        public string Author { get; set; }
        public virtual RecipeType RecipeType { get; set; }
        public virtual IList<RecipeStep> RecipeSteps { get; set; } = new List<RecipeStep>();
    }
}
