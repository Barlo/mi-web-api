﻿namespace Mi.Infrastructure.WebApi.Model.DatabaseModel {
    public class Device : Entity{
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
