﻿using System.Collections.Generic;

namespace Mi.Infrastructure.WebApi.Model.DatabaseModel {
    public class ShoppingList {
        public string DateTime { get; set; }
        public IList<ShoppingElement> Elements { get; set; } = new List<ShoppingElement>();
    }
}
