﻿using Mi.Infrastructure.WebApi.Model.DatabaseModel;

using System.Collections.Generic;

namespace Mi.Infrastructure.WebApi.Model {
    public class Aquarium : Entity{
        public string Name { get; set; }
        public int Size { get; set; }
        public IList<AquariumMeasurement> AquariumMeasurements { get; set; } = new List<AquariumMeasurement>();
    }
}