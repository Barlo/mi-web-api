﻿using System;

namespace Mi.Infrastructure.WebApi.Model.WriteModel {
    public class ToDoViewModel {
        public string Title { get; set; }
        public string Descrition { get; set; }
        public DateTime Deadline{ get; set; }
    }
}
