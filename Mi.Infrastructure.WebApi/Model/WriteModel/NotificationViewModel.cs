﻿using System;

namespace Mi.Infrastructure.WebApi.Model.WriteModel {
    public class NotificationViewModel {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime? Date { get; set; }
    }
}
