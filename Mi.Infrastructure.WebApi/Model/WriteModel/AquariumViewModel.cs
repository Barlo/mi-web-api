﻿namespace Mi.Infrastructure.WebApi.Model.WriteModel {
    public class AquariumViewModel {
        public string Name { get; set; }
        public int Size { get; set; }
    }
}
