﻿namespace Mi.Infrastructure.WebApi.Model.WriteModel {
    public class ConfigurationElementViewModel {
        public string Name { get; set; }
        public string Value { get; set; }
        public string Type { get; set; }
    }
}
