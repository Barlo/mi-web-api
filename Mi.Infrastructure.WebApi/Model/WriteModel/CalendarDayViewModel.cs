﻿using System;

namespace Mi.Infrastructure.WebApi.Model.WriteModel {
    public class CalendarDayViewModel {
        public DateTime Date { get; set; }
    }
}
