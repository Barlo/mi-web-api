﻿using System;

namespace Mi.Infrastructure.WebApi.Model.WriteModel {
    public class DayEventViewModel {
        public string Name { get; set; }
        public bool EntireDay { get; set; }
        public DateTime? TimeFrom { get; set; }
        public DateTime? TimeTo { get; set; }
    }
}
