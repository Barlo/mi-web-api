﻿using AutoMapper;

using Mi.Infrastructure.WebApi.Model.DatabaseModel;
using Mi.Infrastructure.WebApi.Model.WriteModel;

namespace Mi.Infrastructure.WebApi.Model.Profiles {
    public class MappingProfile : Profile {
        public MappingProfile() {
            CreateMap<AquariumViewModel, Aquarium>();
            CreateMap<AquariumMeasurementViewModel, AquariumMeasurement>();
            CreateMap<BillViewModel, Bill>();
            CreateMap<ConfigurationViewModel, Configuration>();
            CreateMap<CalendarDayViewModel, CalendarDay>();
            CreateMap<ConfigurationElementViewModel, ConfigurationElement>();
            CreateMap<DayEventViewModel, DayEvent>();
            CreateMap<DeviceViewModel, Device>();
            CreateMap<NotificationViewModel, Notification>();
            CreateMap<RecipeViewModel, Recipe>();
            CreateMap<RecipeStepViewModel, RecipeStep>();
            CreateMap<RecipeTypeViewModel, RecipeType>();
            CreateMap<ShoppingElementViewModel, ShoppingElement >();
            CreateMap<ShoppingList, ShoppingListViewModel>();
            CreateMap<ToDoViewModel, ToDo>();
        }
    }
}
